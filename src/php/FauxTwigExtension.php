<?php
/**
 * FauxTwigExtension for Craft CMS 3.x
 *
 * This is intended to be used with the Symfony Plugin for PhpStorm:
 * https://plugins.jetbrains.com/plugin/7219-symfony-plugin
 *
 * It will provide full auto-complete for craft.app. and and many other useful things
 * in your Twig templates.
 *
 * Documented in the article:
 * https://panda4man.com/blog/auto-complete-craft-cms-3-apis-in-twig-with-phpstorm
 *
 * Place the file somewhere in your project or include it via PhpStorm Settings -> Include Path.
 * You never call it, it's never included anywhere via PHP directly nor does it affect other
 * classes or Twig in any way. But PhpStorm will index it, and think all those variables
 * are in every single template and thus allows you to use Intellisense auto completion.
 *
 * Thanks to Robin Schambach; for context, see:
 * https://github.com/Haehnchen/idea-php-symfony2-plugin/issues/1103
 *
 * @link      https://panda4man.com
 * @copyright Copyright (c) 2019 panda4man
 * @license   MIT
 * @license   https://opensource.org/licenses/MIT MIT Licensed
 */

namespace panda4man\craft;

use craft\elements\Asset;
use craft\elements\Category;
use craft\elements\Entry;
use craft\elements\Tag;
use craft\elements\User;
use craft\models\Site;
use modules\leaflet\variables\LeafletVariable;

/**
 * Class FauxCraftVariable extends the actual Craft Variable, but with added properties
 * that reflect things that are added to the Craft Variable dynamically by
 * plugins or modules.
 *
 * @property \craft\web\twig\variables\Cp $cp
 * @property \craft\web\twig\variables\Io $io
 * @property \craft\web\twig\variables\Routes $routes
 * @property \craft\web\twig\variables\CategoryGroups $categoryGroups
 * @property \craft\web\twig\variables\Config $config
 * @property \craft\web\twig\variables\Deprecator $deprecator
 * @property \craft\web\twig\variables\ElementIndexes $elementIndexes
 * @property \craft\web\twig\variables\EntryRevisions $entryRevisions
 * @property \craft\web\twig\variables\Feeds $feeds
 * @property \craft\web\twig\variables\Fields $fields
 * @property \craft\web\twig\variables\Globals $globals
 * @property \craft\web\twig\variables\I18N $i18n
 * @property \craft\web\twig\variables\Request $request
 * @property \craft\web\twig\variables\Sections $sections
 * @property \craft\web\twig\variables\SystemSettings $systemSettings
 * @property \craft\web\twig\variables\UserSession $session
 * @property \modules\leaflet\variables\LeafletVariable $leaflet
 * @mixin \craft\commerce\web\twig\CraftVariableBehavior
 *
 * @author    panda4man
 * @package   panda4man\craft
 * @since     1.0.2
 */
class FauxCraftVariable extends \craft\web\twig\variables\CraftVariable
{
}

/**
 * Class FauxTwigExtension provides a faux Twig extension for PhpStorm to index
 * so that we get Intellisense auto-complete in our Twig templates.
 *
 * @author    panda4man
 * @package   panda4man\craft
 * @since     1.0.2
 */
class FauxTwigExtension extends \Twig\Extension\AbstractExtension implements \Twig\Extension\GlobalsInterface
{
    public function getGlobals(): array
    {
        return [
            // Craft Variable
            'craft'              => new FauxCraftVariable(),
            // Craft Elements
            'asset'              => new Asset(),
            'category'           => new Category(),
            'entry'              => new Entry(),
            'tag'                => new Tag(),
            'leaflet'            => new LeafletVariable(),
            // Craft "Constants"
            'SORT_ASC'           => 4,
            'SORT_DESC'          => 3,
            'SORT_REGULAR'       => 0,
            'SORT_NUMERIC'       => 1,
            'SORT_STRING'        => 2,
            'SORT_LOCALE_STRING' => 5,
            'SORT_NATURAL'       => 6,
            'SORT_FLAG_CASE'     => 8,
            'POS_HEAD'           => 1,
            'POS_BEGIN'          => 2,
            'POS_END'            => 3,
            'POS_READY'          => 4,
            'POS_LOAD'           => 5,
            // Misc. Craft globals
            'currentUser'        => new User(),
            'currentSite'        => new Site(),
            'now'                => new \DateTime(),
        ];
    }
}