<?php


namespace modules\leaflet\services;


use Craft;
use craft\base\Component;
use craft\elements\Entry;

class InvoiceService extends Component
{
    /**
     * @param Entry $invoice
     * @return float|int
     */
    public function billableCost(Entry $invoice)
    {
        $rate  = (float)$invoice->invoiceClient->one()->clientRate;
        $hours = $this->sumHours($invoice);

        return $rate * $hours;
    }

    /**
     * @param Entry $invoice
     * @return int
     */
    public function sumHours(Entry $invoice)
    {
        $sum   = 0;
        $works = Entry::find()->section('work')->relatedTo([
            [
                'targetElement' => $invoice,
                'field'         => 'workInvoice'
            ]
        ])->all();

        foreach ($works as $work) {
            $sum += $work->workHours;
        }

        return $sum;
    }

    /**
     * @param Entry $invoice
     * @param Entry $job
     * @return float|int|null
     */
    public function sumHoursForJob(Entry $invoice, Entry $job)
    {
        $sum   = 0;
        $works = Entry::find()->section('work')->relatedTo([
            [
                'targetElement' => $invoice,
                'field'         => 'workInvoice'
            ]
        ])->all();

        foreach ($works as $work) {
            if ($work->workJob->one()->id !== $job->id) {
                continue;
            }

            $sum += $work->workHours;
        }

        return $sum;
    }

    /**
     * @param Entry $invoice
     * @return array
     */
    public function jobs(Entry $invoice)
    {
        $completed_work = Entry::find()
            ->section('work')
            ->relatedTo([
                [
                    'targetElement' => $invoice,
                    'field'         => 'workInvoice'
                ]
            ])
            ->with(['workJob'])
            ->all();
        $jobs           = [];

        foreach ($completed_work as $work) {
            if (!array_key_exists($work->workJob[0]->id ?? null, $jobs)) {
                $jobs[$work->workJob[0]->id] = $work->workJob[0];
            }
        }

        $jobs = array_values($jobs);

        return $jobs;
    }

    /**
     * @param Entry $invoice
     * @return mixed
     */
    public function displayUid(Entry $invoice)
    {
        $parts = explode('-', $invoice->uid);

        return $parts[count($parts) - 1];
    }

    /**
     * @param Entry $invoice
     * @param array $workIds
     * @throws \Throwable
     * @throws \craft\errors\ElementNotFoundException
     * @throws \yii\base\Exception
     */
    public function saveInvoiceWork(Entry $invoice, array $workIds): void
    {
        $query = Entry::find()->section('work')->where(['in', 'elements.id', $workIds]);
        $work  = $query->all();

        foreach ($work as $w) {
            # Skip if work already associated with invoice
            if ($w->workInvoice->one() != null) {
                continue;
            }

            $w->setFieldValue('workInvoice', [$invoice->id]);
            Craft::$app->elements->saveElement($w);
        }
    }
}