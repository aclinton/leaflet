<?php


namespace modules\leaflet\services;


use craft\base\Component;
use craft\elements\Entry;
use Illuminate\Support\Str;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class JsonService extends Component
{
    /**
     * @param Entry $entry
     * @return string
     */
    public function elementToJson(Entry $entry)
    {
        $transformer = $this->_getTransformer($entry);
        $item        = new Item($entry, $transformer);
        $fractal     = new Manager();

        return $fractal->createData($item)->toJson();
    }

    /**
     * @param array $entries
     * @return array
     */
    public function elementsToJson(array $entries)
    {
        $transformer = $this->_getTransformer($entries[0]);
        $collection  = new Collection($entries, $transformer);
        $fractal     = new Manager();

        return $fractal->createData($collection)->toArray();
    }

    /**
     * @param Entry $entry
     * @return TransformerAbstract|string
     */
    private function _getTransformer(Entry $entry)
    {
        $transformer = 'transformers\\' . ucfirst(Str::singular($entry->section->handle)) . 'Transformer';
        /** @var TransformerAbstract $transformer */
        $transformer = new $transformer;

        return $transformer;
    }
}