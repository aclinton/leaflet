<?php


namespace modules\leaflet;


use Craft;
use craft\base\Element;
use craft\events\RegisterCpNavItemsEvent;
use craft\events\RegisterTemplateRootsEvent;
use craft\services\Elements;
use craft\web\twig\variables\Cp;
use craft\web\twig\variables\CraftVariable;
use craft\web\View;
use modules\leaflet\controllers\InvoicePdfsController;
use modules\leaflet\controllers\PreviousInvoiceNumbersController;
use modules\leaflet\controllers\TestController;
use modules\leaflet\services\InvoiceService;
use modules\leaflet\variables\LeafletVariable;
use modules\Module as CraftModule;
use yii\base\Event;

class LeafletModule extends CraftModule
{
    use HasComponents;

    /**
     * @var array
     */
    public $controllerMap = [
        'invoice-pdfs'             => InvoicePdfsController::class,
        'previous-invoice-numbers' => PreviousInvoiceNumbersController::class,
    ];

    public function init()
    {
        Craft::setAlias('@leaflet', __DIR__);

        parent::init();

        $this->setComponents([
            'invoice' => InvoiceService::class
        ]);

        Event::on(
            Cp::class,
            Cp::EVENT_REGISTER_CP_NAV_ITEMS,
            function(RegisterCpNavItemsEvent $event) {
                $event->navItems[] = [
                    'url'    => 'leaflet/invoices',
                    'label'  => 'Leaflet',
                    'subnav' => [
                        'invoices' => [
                            'label' => 'Invoices',
                            'url'   => 'leaflet/invoices'
                        ],
                        'reports'  => [
                            'label' => 'Reports',
                            'url'   => 'leaflet/reports'
                        ]
                    ]
                ];
            }
        );

        Event::on(
            View::class,
            View::EVENT_REGISTER_CP_TEMPLATE_ROOTS,
            function(RegisterTemplateRootsEvent $event) {
                $event->roots['leaflet'] = __DIR__ . '/templates';
            }
        );

        Event::on(CraftVariable::class, CraftVariable::EVENT_INIT, function(Event $e) {
            /** @var CraftVariable $variable */
            $variable = $e->sender;

            // Attach a service:
            $variable->set('leaflet', LeafletVariable::class);
        });

        Event::on(Elements::class, Elements::EVENT_AFTER_SAVE_ELEMENT, function(Event $e) {
            /** @var Element $element */
            $element = $e->element;
            if ($element->hasProperty('section') && $element->section->handle == 'invoices' && ($works = Craft::$app->request->getBodyParam('invoiceWork'))) {
                $this->invoice->saveInvoiceWork($e->element, $works);
            }
        });
    }
}