<?php


namespace modules\leaflet\variables;


class LeafletVariable
{
    /**
     * @param $name
     * @param $arguments
     * @return null
     */
    public function __call($name, $arguments)
    {
        $className = 'modules\\leaflet\\services\\' . ucfirst($name) . 'Service';

        return class_exists($className) ? new $className() : null;
    }
}