<?php


namespace modules\leaflet;


use modules\leaflet\services\InvoiceService;

trait HasComponents
{
    /**
     * @return InvoiceService
     */
    public function invoice(): InvoiceService
    {
        return $this->get('invoice');
    }
}