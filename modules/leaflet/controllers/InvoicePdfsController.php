<?php


namespace modules\leaflet\controllers;


use Craft;
use craft\elements\Entry;
use craft\web\View;
use Dompdf\Dompdf;
use craft\web\Controller;
use yii\web\NotFoundHttpException;

class InvoicePdfsController extends Controller
{
    /**
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\HttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionCreate()
    {
        $this->requirePostRequest();
        $this->requireLogin();

        $invoice = Entry::find()->id(Craft::$app->request->getBodyParam('invoiceId'))->one();

        if ($invoice == null) {
            throw new NotFoundHttpException('Could not find an invoice having ID ' . Craft::$app->request->getBodyParam('invoiceId'));
        }

        $oldMode = Craft::$app->view->getTemplateMode();
        Craft::$app->getView()->setTemplateMode(View::TEMPLATE_MODE_SITE);
        $html = Craft::$app->getView()->renderTemplate('_pdfs/invoice', ['invoice' => $invoice]);
        Craft::$app->view->setTemplateMode($oldMode);
        $dompdf = new Dompdf();

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($invoice->invoiceNumber . "-invoice.pdf");

        Craft::$app->end();
    }
}