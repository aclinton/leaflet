<?php


namespace modules\leaflet\controllers;


use craft\elements\Entry;
use craft\web\Controller;
use yii\web\NotFoundHttpException;

class PreviousInvoiceNumbersController extends Controller
{
    /**
     * @param $clientId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionFind($clientId)
    {
        $invoice = Entry::find()
            ->section('invoices')
            ->relatedTo([
                'field'         => 'invoiceClient',
                'targetElement' => $clientId
            ])
            ->orderBy('invoiceSentAt desc')
            ->one();

        return $this->asJson([
            'data' => [
                'number' => $invoice->invoiceNumber ?? null
            ]
        ]);
    }
}