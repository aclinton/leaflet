<?php

namespace transformers;


use craft\elements\Entry;
use League\Fractal\TransformerAbstract;

class ClientTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = ['projects'];

    /**
     * @param Entry $entry
     * @return array
     */
    public function transform(Entry $entry)
    {
        return [
            'id'      => $entry->id,
            'title'   => $entry->title,
            'logo'    => $entry->clientLogo ? $entry->clientLogo->one()->url ?? null : null,
            'rate'    => $entry->clientRate,
            'address' => [

            ]
        ];
    }

    /**
     * @param Entry $entry
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProjects(Entry $entry)
    {
        $query = Entry::find();
        $query
            ->section('projects')
            ->relatedTo([
                'targetElement' => $entry,
                'field'         => 'projectClient'
            ]);

        return $this->collection($query->all(), new ProjectTransformer());
    }
}