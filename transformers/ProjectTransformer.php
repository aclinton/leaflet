<?php


namespace transformers;


use craft\elements\Entry;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = ['jobs', 'client'];

    /**
     * @param Entry $entry
     * @return array
     */
    public function transform(Entry $entry)
    {
        return [
            'id'    => $entry->id,
            'title' => $entry->title,
        ];
    }

    /**
     * @param Entry $entry
     * @return \League\Fractal\Resource\Item
     */
    public function includeClient(Entry $entry)
    {
        return $this->item($entry->projectClient->one(), new ClientTransformer());
    }

    /**
     * @param Entry $entry
     * @return \League\Fractal\Resource\Collection
     */
    public function includeJobs(Entry $entry)
    {
        $query = Entry::find();
        $query
            ->section('jobs')
            ->relatedTo([
                'targetElement' => $entry,
                'field'         => 'jobProject'
            ]);

        return $this->collection($query->all(), new JobTransformer());
    }
}