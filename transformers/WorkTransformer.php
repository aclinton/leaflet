<?php


namespace transformers;


use craft\elements\Entry;
use League\Fractal\TransformerAbstract;

class WorkTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = ['job'];

    /**
     * @param Entry $entry
     * @return array
     */
    public function transform(Entry $entry)
    {
        return [
            'id'          => $entry->id,
            'title'       => $entry->title,
            'hours'       => $entry->workHours,
            'date'        => $entry->workDate->format('Y-m-d'),
            'description' => $entry->workDescription
        ];
    }

    /**
     * @param Entry $entry
     * @return \League\Fractal\Resource\Item
     */
    public function includeJob(Entry $entry)
    {
        return $this->item($entry->workJob->one(), new JobTransformer());
    }
}