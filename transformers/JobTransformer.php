<?php


namespace transformers;


use craft\elements\Entry;
use League\Fractal\TransformerAbstract;

class JobTransformer extends TransformerAbstract
{
    /**
     * @param Entry $entry
     * @return array
     */
    public function transform(Entry $entry)
    {
        return [
            'id'    => $entry->id,
            'title' => $entry->title
        ];
    }
}