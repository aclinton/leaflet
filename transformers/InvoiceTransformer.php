<?php


namespace transformers;


use craft\elements\Entry;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use modules\leaflet\LeafletModule;

class InvoiceTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = ['work', 'client'];

    /**
     * @var array
     */
    protected $defaultIncludes = ['client', 'work'];

    /**
     * @param Entry $entry
     * @return array
     */
    public function transform(Entry $entry)
    {
        return [
            'uid'          => $entry->uid,
            'display_uid'  => LeafletModule::getInstance()->invoice->displayUid($entry),
            'number'       => $entry->invoiceNumber,
            'sent'         => $entry->invoiceSent ? true : false,
            'paid'         => $entry->invoicePaid ? true : false,
            'hours_worked' => LeafletModule::getInstance()->invoice->sumHours($entry)
        ];
    }

    /**
     * @param Entry $entry
     * @return Collection
     */
    public function includeWork(Entry $entry)
    {
        return $this->collection($entry->invoiceWork, new WorkTransformer());
    }

    /**
     * @param Entry $entry
     * @return Item
     */
    public function includeClient(Entry $entry)
    {
        return $this->item($entry->invoiceClient->one(), new ClientTransformer());
    }
}