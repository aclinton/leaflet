import Vue from 'vue';

const app = new Vue({
    delimiters: ['${', '}'],
}).$mount('#app');

import './bootstrap.js';