import Axios from 'axios';
import Vue from 'vue';
import 'bootstrap';
import 'popper.js';

Vue.prototype.$http = Axios.create();
Vue.prototype.$http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
Vue.prototype.$http.defaults.headers.common['X-CSRF-TOKEN'] = window.Craft.csrfTokenValue;

import './components/bootstrap';