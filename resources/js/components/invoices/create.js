import Vue from 'vue';

Vue.component('invoice-create', {
    props: ['clients'],
    data() {
        return {
            http: {
                work: false,
                projects: false,
                invoiceNumber: false
            },
            work: [],
            projects: [],
            clientId: '',
            projectId: [],
            workIds: [],
            workCheckAll: false,
            recentInvoiceNumber: null
        };
    },
    watch: {
        clientId(val) {
            if (val) {
                this.getProjects();
                this.getMostRecentInvoiceNumber(val);
            } else {
                this.projects = [];
            }
        },
        projectId(val) {
            const jobs = this.getSearchableJobs();

            this.getWork(jobs);
        },
        workCheckAll(val) {
            if(!val) {
                this.workIds = [];
            } else {
                this.workIds = this.work.map(w => w.id);
            }
        }
    },
    methods: {
        getProjects() {
            this.http.projects = true;

            this.$http.get(`/clients/${this.clientId}/projects.json?include=jobs,client`, {}).then(res => {
                this.projects = res.data.data;
                this.projectId = [];
            }).catch(res => {
                console.log(res.data);
            }).finally(() => {
                this.http.projects = false;
            });
        },
        getWork(jobs = []) {
            if(jobs.length < 1) {
                this.work = [];
                this.workIds = [];
                return;
            }

            this.http.work = true;
            let params = {
                include: 'job'
            };

            if (jobs && jobs.length) {
                params.jobs = jobs.join(',');
            }

            this.$http.get('/work.json', {params}).then(res => {
                this.work = res.data.data;
                this.workIds = [];
            }).catch(res => {
                console.log(res.data);
            }).finally(() => {
                this.http.work = false;
            });
        },
        getSearchableJobs() {
            let jobs = [];

            if (!this.projectId || this.projectId.length < 1) {
                this.projects.forEach(p => {
                    jobs = jobs.concat(p.jobs.data.map(j => j.id));
                });

                return jobs;
            }

            this.projects.forEach(p => {
                let found = this.projectId.some(id => {
                    return id == p.id;
                });

                if (found) {
                    jobs = jobs.concat(p.jobs.data.map(j => j.id));
                }
            });

            return jobs;
        },
        getWorkRate(work) {
            let rate = null;

            this.projects.forEach(p => {
                let j = p.jobs.data.filter(j => {
                    return j.id == work.job.id;
                });

                if(j.length) {
                    rate = p.client.rate;
                }
            });

            return rate;
        },
        getMostRecentInvoiceNumber(clientId) {
            this.http.invoiceNumber = true;

            this.$http.get(`/actions/leaflet/previous-invoice-numbers/find?clientId=${clientId}`).then(res => {
                this.recentInvoiceNumber = res.data.data.number || '001';
            }).catch(res => {

            }).finally(() => {
                this.http.invoiceNumber = false;
            });
        }
    },
    computed: {
        totalHours() {
            return this.work.filter(w => {
                return this.workIds.indexOf(w.id) > -1;
            }).reduce((sum, w) => {
                return sum += parseFloat(w.hours);
            }, 0);
        },
        totalCost() {
            const client = this.clients.data.find(c => {
                return c.id == this.clientId;
            });

            if(client == undefined) {
                return 0;
            }

            return parseFloat(client.rate) * this.totalHours;
        }
    }
});