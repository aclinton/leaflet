import Vue from 'vue';

Vue.component('invoices-all', {
    data() {
        return {
            http: {
                invoices: false
            },
            invoices: [],
            table: {
                headers: [
                    {
                        name: 'uid',
                        text: 'ID'
                    },
                    {
                        name: 'number',
                        text: 'Number'
                    },
                    {
                        name: 'client',
                        text: 'Client'
                    },
                    {
                        name: 'hours',
                        text: 'Hours'
                    },
                    {
                        name: 'invoiced',
                        text: 'Invoiced'
                    },
                    {
                        name: 'paid',
                        text: 'Paid'
                    }
                ]
            }
        }
    },
    created() {
        this.getInvoices();
    },
    methods: {
        getInvoices() {
            this.http.invoices = true;

            this.$http.get('/invoices.json').then(res => {
                this.invoices = res.data.data;
            }).catch(res => {

            }).finally(() => {
                this.http.invoices = false;
            });
        }
    }
});