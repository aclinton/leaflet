const mix = require('laravel-mix');

mix
    .js('resources/js/app.js', 'web/js/app.js')
    .sass('resources/sass/app.scss', 'web/css/app.css')
    .sourceMaps()
    .setPublicPath('web');