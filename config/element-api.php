<?php

use craft\elements\Entry;
use transformers\ClientTransformer;
use transformers\InvoiceTransformer;
use transformers\ProjectTransformer;
use transformers\WorkTransformer;

return [
    'endpoints' => [
        'clients.json'                         => function() {
            return [
                'elementType' => Entry::class,
                'criteria'    => ['section' => 'clients'],
                'includes'    => Craft::$app->request->getQueryParam('include', []),
                'transformer' => new ClientTransformer()
            ];
        },
        'invoices.json'                        => function() {
            return [
                'elementType' => Entry::class,
                'criteria'    => ['section' => 'invoices'],
                'transformer' => new InvoiceTransformer(),
            ];
        },
        'clients/<clientId:\d+>/projects.json' => function($clientId) {
            $criteria = [
                'section'   => 'projects',
                'relatedTo' => [
                    'targetElement' => $clientId
                ]
            ];

            return [
                'elementType' => Entry::class,
                'criteria'    => $criteria,
                'includes'    => Craft::$app->request->getQueryParam('include', []),
                'transformer' => new ProjectTransformer()
            ];
        },
        'work.json'                            => function() {
            $relatedTo = array_map('trim', explode(',', Craft::$app->request->getQueryParam('jobs', null)));
            $criteria  = [
                'section'     => 'work',
                'workInvoice' => ':empty:'
            ];

            if (count($relatedTo) && !empty($relatedTo[0])) {
                $elements = array_map(function($id) {
                    return [
                        'targetElement' => $id
                    ];
                }, $relatedTo);

                $criteria['relatedTo'][] = 'or';

                foreach ($elements as $el) {
                    $criteria['relatedTo'][] = $el;
                }
            }

            return [
                'elementType' => Entry::class,
                'criteria'    => $criteria,
                'includes'    => Craft::$app->request->getQueryParam('include', []),
                'transformer' => new WorkTransformer()
            ];
        },
    ]
];